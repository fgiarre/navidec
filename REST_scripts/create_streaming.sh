#!/bin/bash
tmp="http://$1:5000/deployments"
curl -XPOST -H "Content-Type: text/plain" --data "manifests/streaming.yaml" $tmp

tmp="http://$1:5000/services"
curl -XPOST -H "Content-Type: text/plain" --data "manifests/streaming-service.yaml" $tmp
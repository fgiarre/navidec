#!/bin/bash

kubectl delete deployment streaming
kubectl delete deployment classifier
kubectl delete service streaming-service
kubectl delete service classifier-service

rm /etc/nginx/sites-enabled/streaming-service.conf
rm /etc/nginx/sites-enabled/classifier-service.conf

nginx -s reload
#!/usr/bin/python
"""
This is the most simple example to showcase Containernet.
"""
from mininet.net import Containernet
from mininet.node import Controller
from mininet.cli import CLI
from mininet.link import TCLink
from mininet.log import info, setLogLevel
import os
setLogLevel('info')

net = Containernet(controller=Controller)
info('*** Adding controller\n')
net.addController('c0')
info('*** Adding docker containers\n')
d1 = net.addDocker('d1', ip='10.0.0.251', dimage="cnet_nind",dcmd="./startup-nind.sh",environment={"CLUSTERNAME": "cluster1", "API_IP": "10.0.0.251","BROKER_IP":"10.0.0.254","POSITION":"deep-edge"}, volumes=["/home/federico/navidec:/navidec"])
d2 = net.addDocker('d2', ip='10.0.0.252', dimage="cnet_nind",dcmd="./startup-nind.sh",environment={"CLUSTERNAME": "cluster2", "API_IP": "10.0.0.252","BROKER_IP":"10.0.0.254","POSITION":"edge"}, volumes=["/home/federico/navidec:/navidec"])
#d3 = net.addDocker('d3', ip='10.0.0.253', dimage="cnet_nind",dcmd="./startup-nind.sh",environment={"CLUSTERNAME": "cluster3", "API_IP": "10.0.0.253","BROKER_IP":"10.0.0.254"}, volumes=["/home/federico/navidec:/navidec"])

broker = net.addDocker('broker', ip='10.0.0.254', dimage="cnet_rabbitmq",dcmd="./startup-rabbitmq.sh",environment={"RABBITMQ_DEFAULT_USER": "navidec", "RABBITMQ_DEFAULT_PASS": "navidec"})
info('*** Adding switches\n')
s1 = net.addSwitch('s1')
s2 = net.addSwitch('s2')
info('*** Creating links\n')
h1 = net.addHost("h1",ip="10.0.0.201")
net.addLink(h1,s1, cls=TCLink)
net.addLink(d1, s1, cls=TCLink)
net.addLink(d2, s2, cls=TCLink)
#net.addLink(d3, s2)
net.addLink(s1, s2, cls=TCLink,bw=10)
net.addLink(s2, broker, cls=TCLink)
info('*** Starting network\n')
net.start()
info('*** Running CLI\n')
CLI(net)
info('*** Stopping network')
net.stop()

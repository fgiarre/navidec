from flask import Flask,  render_template, jsonify
from flask import request, Response
from flask_cors import CORS

from markupsafe import escape

import subprocess
import os
import json
import time
import threading
import pika
import uuid
import sys
import requests

# -- Importing scripts
from scripts.utils import * 
from scripts.pods import *
from scripts.deployments import *
from scripts.services import *
import scripts.config as cfg




app = Flask(__name__,template_folder='pages')
app.jinja_env.auto_reload = True
app.config['TEMPLATES_AUTO_RELOAD'] = True
CORS(app)



# ----  APIs -----       

#Return info about the cluster
@app.route("/")
def index():
    cmd = " kubectl cluster-info"
    result = subprocess.check_output(cmd, shell=True,universal_newlines=True)
    
    return f"{result}",200

#Return info about the nodes in the cluster
@app.route("/nodes",methods=['GET'])
def nodes_get():
    cmd = " kubectl get nodes"
    tmp = subprocess.check_output(cmd, shell=True,universal_newlines=False).decode('utf-8').splitlines()
    result = {"data":[]}
    for e in tmp:
       result["data"].append(" ".join(e.split()))
    return f"{json.dumps(result)}",200

@app.route("/resources/<string:name>",methods=['PATCH'])
def updateAbroadResource_wrapper(name):
    return updateAbroadResource(request,name)

# ---- Pod APIs ----- 

#Create a new pod starting from a file, json or internet resource
@app.route("/pods",methods=['POST']) # data structure: {'name': <name pod>,<flag>:<value>*,...}
def pod_add_json_wrapper():
   return pod_add_json(request)

#Get all pods - Adding the "abroad" argument will also retrieve any pod in abroadResources
@app.route("/pods",methods=['GET'])
def pods_get_wrapper():
    return pods_get(request)

#Get data about a specific pod
@app.route("/pods/<string:name>",methods=['GET'])
def pod_get_wrapper(name):
    return pod_get(name)

#Delete a specific pod
@app.route("/pods/<string:name>",methods=['DELETE'])
def pod_delete_wrapper(name):
    return pod_delete(name)

#Move a certain pod to another node inside the same cluster where it resides 
@app.route("/pods/<string:name>/move_specific",methods=['POST'])
def pod_move_specific_wrapper(name):
    return pod_move_specific(name,request)

#Add required or preference affinity to a pod
@app.route("/pods/<string:name>/move_affinity",methods=['POST']) #{'name':' name', 'type':'required', 'matchExpressions': ['key': key, 'operator': op, 'values':[val1, val2]]}
def pod_move_affinity_wrapper(name):
    return pod_move_affinity(name,request)

# ---- Deployment APIs -----

#Get all deployments - Adding the "abroad" argument will also retrieve deployments registered in abroadResources
@app.route("/deployments",methods=['GET'])
def deployments_get_wrapper():
    return deployments_get(request)

@app.route("/deployments/<string:name>",methods=["GET"])
def deployment_get_wrapper(name):
    return deployment_get(name)

#Add a new deployment by file, json or internet resource
@app.route("/deployments",methods=['POST']) 
def deployment_add_wrapper():
   return deployment_add(request)

#Delete a deployment by name
@app.route("/deployments/<string:name>",methods=['DELETE'])
def deployment_delete_wrapper(name):
    return deployment_delete(name)

@app.route("/deployments/<string:name>/move_affinity",methods=['POST'])
def deployment_move_affinity_wrapper(name):
    return deployment_move_affinity(name,request)


# ---- Service APIs ------
#Get services - Adding the "abroad" argument will also retrieve deployments registered in abroadResources
@app.route("/services",methods=['GET'])
def services_get_wrapper():
    return services_get(request)
@app.route("/services",methods=['POST']) 
def services_add_wrapper():
   return services_add(request)
@app.route("/services/<string:name>",methods=['DELETE'])
def service_delete_wrapper(name):
    return service_delete(name)

# ---- NaviWeb related APIs -----

@app.route("/webapi",methods=['GET'])
def web_index():
    return render_template('home.html')
@app.route("/webapi/pod",methods=['GET'])
def web_pod():
    name = request.args.get('name')
    return render_template('pod.html')


# ---- Other parameters and instructions ------

#abroadResources={'deployment': {'type': 'deployment', 'host': '192.168.122.212', 'API-port': '5000', 'missed-heartbeat': 0}}

updateOrchestrator(cfg.name,cfg.ip,cfg.port,getLabels(),cfg.position)
threading.Thread(target=garbageCollector,args=(abroadResources,)).start() #Start the garbage collector thread
threading.Thread(target=bandwidth_checker).start()

#!/bin/bash

SERVICE_URL="http://10.0.0.251:30081"  # Replace with your service URL
DELAY_SECONDS=5  # Adjust the delay between each HTTP call

while true; do
    response=$( curl -s -o /dev/null -w "%{http_code}" http://10.0.0.251:30081)
    echo "$(($(date +%s%N)/1000000)),$response"
done

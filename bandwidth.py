import subprocess
import sys
import argparse
import re

"""
Set the bandwidth for the link intf1 - intf2 to the specified bandwidth
"""

parser = argparse.ArgumentParser(description='Set the bandwidth for the link intf1 - intf2 to the specified bandwidth, loss, latency')
parser.add_argument("interface1", metavar='i1', type=str, help='First interface')
parser.add_argument("interface2", metavar='i2', type=str, help='Second interface')
parser.add_argument("-bw", "--bandwidth",metavar='bw', type=str, help='Bandwidth to be set with unit between 5kbps and 1gbit to be safe')
parser.add_argument("-ls","--loss", metavar='loss', type=str, help='Loss to be set in percentage i.e. 10')
parser.add_argument("-lt","--latency", metavar='latency', type=str, help='Latency to be set in ms')
    
args = parser.parse_args()

bw = ""
latency = ""

netem1 = f"sudo tc qdisc replace dev {args.interface1} parent 1: handle 2: netem"
netem2 = f"sudo tc qdisc replace dev {args.interface2} parent 1: handle 2: netem"

if not args.bandwidth:
    bw = "1gbit"
else:
    bw = args.bandwidth

if args.latency:

    netem1+=f" delay {args.latency}ms"
    netem2+=f" delay {args.latency}ms"

#Config loss for intf1
if args.loss:
    netem1 += f" loss {args.loss}%"
    netem2 += f" loss {args.loss}%"


cmd = f"sudo tc qdisc replace dev {args.interface1} root handle 1: tbf rate {bw} burst 15k latency 1ms && {netem1}"
subprocess.check_output(cmd,shell=True)

cmd = f"sudo tc qdisc replace dev {args.interface2} root handle 1: tbf rate {bw} burst 15k latency 1ms && {netem2}"
subprocess.check_output(cmd,shell=True)
match = re.search(r'(\d+)\s*(\w+)', bw)
if match:
    
    value = int(match.group(1))
    unit = match.group(2).lower()
    if unit == 'gbit':
        value *= 1024 * 1024  # convert to Mbit
    elif unit == 'mbit':
        value *= 1024
    
        

    f = open("bandwidth.log", "w")
    f.write('{ "bw":"'+str(value)+'" }')
    f.close()

else:
    raise ValueError("Invalid string format")



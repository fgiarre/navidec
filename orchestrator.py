#!/usr/bin/env python
import networkx as nx
import pika
import json
import time
import threading

G = nx.Graph()
clusters={}
credentials = pika.PlainCredentials('navidec', 'navidec')
connection = pika.BlockingConnection(pika.ConnectionParameters(host="10.0.0.254",credentials=credentials))
channel = connection.channel()

channel.queue_declare(queue='rpc_queue')
channel.queue_declare(queue='join_queue')
channel.queue_purge('rpc_queue')
channel.queue_purge('join_queue')

def requiredHelper(expressions):
    composedExpr = []
    elegibleClusters = []
    for e in expressions:
        for m in e["matchExpressions"]:
            operator = m["operator"]
            #Define specific operation per operator
            if operator == "In":
                for v in m["values"]:
                    composedExpr.append(f"{m['key']}={v}")
    for c in clusters:
        if all(item in clusters[c]["labels"] for item in composedExpr):
            elegibleClusters.append(c)
    return elegibleClusters

def podHelper(pod):
    elegibleClusters = []
    if "spec" in pod and "affinity" in pod["spec"] and "requiredDuringSchedulingIgnoredDuringExecution" in pod['spec']["affinity"]["nodeAffinity"]:
        expr =   pod['spec']["affinity"]["nodeAffinity"]["requiredDuringSchedulingIgnoredDuringExecution"]["nodeSelectorTerms"]
        elegibleClusters = requiredHelper(expr)

    #Optimization choice between elegibleClusters
    if not elegibleClusters:
        return("No elegible clusters")
    else:

        return f"{clusters[elegibleClusters[0]]['ip']}:{clusters[elegibleClusters[0]]['API-port']}"
def deploymentHelper(depl):
    elegibleClusters = []
    if   "affinity" in depl["spec"]["template"]["spec"] and "nodeAffinity" in depl["spec"]["template"]["spec"]["affinity"] and "requiredDuringSchedulingIgnoredDuringExecution" in depl["spec"]["template"]["spec"]["affinity"]["nodeAffinity"]:
        expr =   depl["spec"]["template"]["spec"]["affinity"]["nodeAffinity"]["requiredDuringSchedulingIgnoredDuringExecution"]["nodeSelectorTerms"]
        elegibleClusters = requiredHelper(expr)
    #Optimization choice between elegibleClusters
    if not elegibleClusters:
        return("No elegible clusters")
    else:

        return f"{clusters[elegibleClusters[0]]['ip']}:{clusters[elegibleClusters[0]]['API-port']}"

def bandwidth_helper():
    elegibleClusters = []
    for e in clusters:
        if clusters[e]["position"] == "edge":
            elegibleClusters.append(e)
    if not elegibleClusters:
        return("No elegible clusters")
    else:
        return f"{clusters[elegibleClusters[0]]['ip']}:{clusters[elegibleClusters[0]]['API-port']}"

def decision(body):
    res = ""
    b = json.loads(body.decode("utf-8"))
    print(type(b))
    if "affinity" in b["spec"]["template"]["spec"]:
        if b["kind"] == "Pod":
            res = podHelper(b)
        if b["kind"] == "Deployment":
            res = deploymentHelper(b)
    else:
        if b["kind"] == "Deployment":
            res = bandwidth_helper()


    return res
    
    

def join_onrequest(ch, method, properties, body):
    
    b = json.loads(body)
    name = b["name"]
    ip = b["ip"]
    port = b["port"]
    l = b["labels"]
    cpu = b["cpu"]
    mem = b["mem"]
    pos = b["position"]
    clusters[name] = {"ip":ip,"API-port":port,"labels":l,"cpu":cpu,"mem":mem,"hello":0,"position":pos}
    G.add_node(name)
    ch.basic_ack(delivery_tag=method.delivery_tag)

def rpc_onrequest(ch, method, props, body):
    
    response = decision(body)

    ch.basic_publish(exchange='',
                     routing_key=props.reply_to,
                     properties=pika.BasicProperties(correlation_id = props.correlation_id),
                     body=str(response))
    ch.basic_ack(delivery_tag=method.delivery_tag)

def garbageCollector():
    while True:
        if clusters:
            print("Available Clusters\n")
            for e in clusters:
                print(f"\tName: {e} Missed-Hello: {clusters[e]['hello']}\n")
        to_delete = []
        for cluster in clusters:
            if clusters[cluster]["hello"]>5:
                to_delete.append(cluster)
            clusters[cluster]["hello"]+=1
        for e in to_delete:
            clusters.pop(e)
        time.sleep(10)
        
x = threading.Thread(target=garbageCollector) #Start the garbage collector thread
x.start()

channel.basic_qos(prefetch_count=1)
channel.basic_consume(queue='rpc_queue', on_message_callback=rpc_onrequest)
channel.basic_consume(queue='join_queue', on_message_callback=join_onrequest)

print(" [x] Awaiting RPC requests")
channel.start_consuming()


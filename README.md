# NaviDEC

Presentation: https://docs.google.com/presentation/d/1FReZj_slKJGa7bad17NiRGsSg2b9uXnl7nU0w5l-9tA/edit?usp=sharing

The project consist in being able to handle and move resources between different kubernetes clusters using  REST APIs (in a transparent way for the user). 

## Features
 - Possibility to handle any resource inside of a single cluster
 - It is possible to move resources from one cluster to another
 - A meta-orchestrator decides where to move the resources
 - The client remains agnostic of the movement, continuing to refer to the "home" cluster

## Architecture
For the testbed we implemented two clusters, which expose the REST APIs, and a meta orchestrator (along with the message broker, in this case RabbitMQ)
![alt text](img/v2.png "Version 2 complete architecture")

## Abroad Resources
A resource which is moved from one cluster (the "home" cluster) to another (the "abroad" cluster) is considered as an Abroad Resource. Specifically, the home cluster will take care of checking that the resource is still alive in the abroad cluster and of forwarding any API request of a client regarding that resource to the abroad cluster.
![alt text](img/abroad.png "Abroad resrouces management")

# Configuration

- Clone the repository on the master node of each cluster
- Start the meta orchestrator `orchestrator.py` on the meta orchestrator machine
- On the Master nodes:
    - Edit the `scripts/config.py` with the wanted configuration (mostly name, ip and port but you should also configure any RabbitMQ credentials/ip if changed)
    - Start the API server by cli using the command `sudo flask --app k3s-api.py run --host <desired interface to listen to or 0.0.0.0> --port <desired port>`
    - Make sure the port set on the flask command and config file are the same
- The API is now running you can use curl or any HTTP request sender to contact it

## Current Testbed

One physical server (vapnik) and two virtual machine masters (dam and eurm). The two cluster are composed by two worker nodes (also VMs) and their master.

The meta orchestrator is deployed on the vapnik server, to serve as an external entity, the API are executed on each master

The RabbitMQ is deployed on vapnik itself

vapnik and the two masters have IPs on a subnet to connect with each other.

### Nodes and addresses

- vapnik:
    - IP:  `192.168.122.1`
- dam:
    - IP: `192.168.122.211`
    - api-port: `5000`
- eurm:
    - IP: `192.168.122.212`
    - api-port: `5000`

### To connect from personal laptop to DAM APIs

- From the personal laptop's cli input `ssh -L 0.0.0.0:5000:192.168.122.211:5000 vapnik` to forward any traffic of the localhost to the dam's IP address
- Start the meta-orchestrator on vapnik
- Start the API on DAM
- It is now possible to execute API calls such as `curl http://localhost:5000/pods` from your personal laptop


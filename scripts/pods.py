import subprocess
import json
import pika
import time
import requests
from scripts.config import *
from scripts.utils import *

#Create a new pod starting from a file, json or internet resource
def pod_add_json(request):
    result = ""
    try:
        content = json.loads(json.dumps(request.data.decode('utf-8')))
        cmd = f"echo '{content}' | kubectl apply -f -  "
        result = subprocess.check_output(cmd, shell=True,universal_newlines=True)
    except:
        content = request.data.decode('utf-8')
        cmd = f" kubectl create -f {content} "
        result = subprocess.check_output(cmd, shell=True,universal_newlines=True)
        
    podname = result.split(" ")[0].split("/")[1]
    cmd = f" kubectl get pod {podname} -o json"
    pod = json.loads(subprocess.check_output(cmd, shell=True,universal_newlines=True))

   
    if "home_address" in request.args:
        home = request.args.get("home_address")
        ip = home.split(":")[0]
        port = home.split(":")[1]
        name = result.split("/")[1].split(" ")[0]
        foreignResources[name] = {"type":"pod","home-host":ip,"home-API-port":port}

    if not podHelper(pod):
        move_pod(podname)

    return result,200

    

#Get all pods - Adding the "abroad" argument will also retrieve any pod in abroadResources
def pods_get(request):
    cmd = " kubectl get pods -o wide"
    tmp = subprocess.check_output(cmd, shell=True,universal_newlines=False).decode('utf-8').splitlines()
    result = {"data":[]}
    for e in tmp:
        result["data"].append(" ".join(e.split()))
    if request.args.get("abroad") == "True":
        print("Printing abroad resources")
        ar_set = set()
        for e in abroadResources:
            if abroadResources[e]["host"] not in ar_set:
                ar_set.add(abroadResources[e]["host"])
                res = requests.get("http://"+abroadResources[e]["host"]+":"+abroadResources[e]["API-port"]+"/pods")
                if res.status_code == 200:
                    tmp = json.loads(res.content.decode('utf-8'))
                    print(tmp)
                    for m in tmp["data"]:
                        if m.split(" ")[0] != "NAME":
                            result["data"].append(m)
            
    return f"{json.dumps(result)}",200

#Get data about a specific pod
def pod_get(name):
    if name not in abroadResources:
        cmd = f" kubectl get pod {name} -o json"
        pod = json.loads(subprocess.check_output(cmd, shell=True,universal_newlines=True))
    else:
        res = requests.get("http://"+abroadResources[name]["host"]+":"+abroadResources[name]["API-port"]+"/pods/"+name)
        pod = json.loads(res.content.decode('utf-8'))
    return json.dumps(pod),200

#Delete a specific pod
def pod_delete(name):
    if name not in abroadResources:
        cmd = f" kubectl delete pod {name} "
        result = subprocess.check_output(cmd, shell=True,universal_newlines=True)
        if name in foreignResources:
            foreignResources.pop(name)
    else:
        res = requests.delete("http://"+abroadResources[name]["host"]+":"+abroadResources[name]["API-port"]+"/pods/"+name)
        result = res.content.decode('utf-8')
    return f"{result}",200

#Move a certain pod to another node inside the same cluster where it resides 
def pod_move_specific(name,request):
    content = json.loads(request.data.decode('utf-8'))
    if name in abroadResources:
        result = requests.post("http://"+abroadResources[name]["host"]+":"+abroadResources[name]["API-port"]+"/pods/"+name+"/move_specific",json=content)
    else:
        destination = content['destination']

    
        cmd = f" kubectl get pod {name} -o json"
        pod = json.loads(subprocess.check_output(cmd, shell=True,universal_newlines=True))

        if 'nodeName' in pod['spec']:
            pod['spec'].pop('nodeName')
        
        if 'affinity' in pod['spec']:
            pod['spec'].pop('affinity')
        
        pod['spec']['nodeName'] = destination
        cmd = f" kubectl delete pod {name}"
        subprocess.check_output(cmd, shell=True,universal_newlines=True)
        cmd = f"echo '{json.dumps(pod)}' |  kubectl create -f -"
        result = subprocess.check_output(cmd, shell=True,universal_newlines=True)
    return f"{result}",200

#Add required or preference affinity to a pod
def pod_move_affinity(name,request):
    content = json.loads(request.data.decode('utf-8'))

    if name in abroadResources:
        result = requests.post("http://"+abroadResources[name]["host"]+":"+abroadResources[name]["API-port"]+"/pods/"+name+"/move_affinity",json=content)
    else:
        expressions = content['matchExpressions']
        
        cmd = f" kubectl get pod {name} -o json"
        pod = json.loads(subprocess.check_output(cmd, shell=True,universal_newlines=True))
        
        if 'nodeName' in pod['spec']:
            pod['spec'].pop('nodeName')
        
        if 'affinity' in pod['spec']:
            pod['spec'].pop('affinity')
        affinity = {"nodeAffinity":{}}
        if 'type' in content and content['type']=="required":
            affinity["nodeAffinity"] = {"requiredDuringSchedulingIgnoredDuringExecution":{"nodeSelectorTerms":[{"matchExpressions":expressions}]}}
        if 'type' in content and content['type']=="preferred":
            affinity["nodeAffinity"] = {"preferredDuringSchedulingIgnoredDuringExecution":[{"weight":1,"preference":{"matchExpressions":expressions}}]}
        pod['spec']['affinity'] = affinity
        cmd = f" kubectl delete pod {name}"
        subprocess.check_output(cmd, shell=True,universal_newlines=True)
        cmd = f"echo '{json.dumps(pod)}' |  kubectl create -f -"
        result = subprocess.check_output(cmd, shell=True,universal_newlines=True)
        
    return f"{result}",200

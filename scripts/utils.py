import subprocess
import json
import pika
import time
import uuid
import requests
from datetime import datetime

from scripts.config import *

        

def updateAbroadResource(request,name):
    j = json.loads(request.data.decode('utf-8'))
    resname = j["name"]
    resip = j["host"]
    resport = j["API-port"]
    if name in abroadResources:
        abroadResources[resname]["host"]=resip
        abroadResources[resname]["API-port"]=resport

    return abroadResources[resname],200

#Check if the pod belongs to a deployment, if it belongs to one return the name of it
def isADeploymentPod(podname): 
    cmd = f" kubectl get pod {podname} -o json "
    try:
        pod = json.loads(subprocess.check_output(cmd, shell=True,universal_newlines=True))
    except:
        return False
    if "metadata" in pod and "generateName" in pod["metadata"]:
        tmp = pod["metadata"]["generateName"].split("-")
        deploymentName = ""
        for i,e in enumerate(tmp):
            
            if i!= len(tmp)-2:
                deploymentName+=e
        return deploymentName
    else:
        return False

def hasAService(dep):
    appname = ""
    if "labels" in dep["metadata"] and "app" in dep["metadata"]["labels"]:
        appname = dep["metadata"]["labels"]["app"]
        cmd = " kubectl get services -o wide"
        tmp = subprocess.check_output(cmd, shell=True,universal_newlines=False).decode('utf-8').splitlines()
        for e in tmp:
            rec = " ".join(e.split())
            if rec.split(" ")[6] == f"app={appname}":
                return rec.split(" ")[0]
        return False


#Get all the labels related to the cluster 
def getLabels(): 
    global availableLabels
    cmd = " kubectl get nodes --show-labels"
    tmp = subprocess.check_output(cmd, shell=True,universal_newlines=False).decode('utf-8').splitlines()
    availableLabels = []
    par = []
    for i, e in enumerate(tmp):
        if i!=0:
            k = " ".join(e.split())
            for q in k.split(" ")[5].split(","):
                availableLabels.append(q)

    #return json.dumps({"name":name,"ip":ip,"port":port,"labels":availableLabels})
    return availableLabels

#Get the RPC response of the meta-orchestrator
def rpcResponse(ch, method, props, body):
    global rpc_response, corr_id
    if corr_id == props.correlation_id:
            rpc_response = body

#Ask the meta-orchestrator for a suitable cluster to run the pod
def ask_controller(pod):
    global rpc_response, corr_id
    rpc_response = None
    
    channel.basic_publish(
        exchange='',
        routing_key='rpc_queue',
        properties=pika.BasicProperties(
            reply_to=callback_queue,
            correlation_id=corr_id,
        ),
        body=json.dumps(pod))
    connection.process_data_events(time_limit=None)
    return rpc_response.decode("utf-8")


#If a pod is unschedulable get the address of a suitable cluster, spawn the pod there and delete the pod from the actual cluster
def move_pod(podname):
    cmd = f" kubectl get pod {podname} -o json "
    try:
        pod = json.loads(subprocess.check_output(cmd, shell=True,universal_newlines=True))
    except:
        return
    if  pod["status"]["conditions"][0]["reason"] == "Unschedulable":
        destination = ask_controller(pod)
        req = "http://"+destination+"/pods?home_address="
        if podname in foreignResources:
            req = req+f"{foreignResources[podname]['home-host']}:{foreignResources[podname]['home-API-port']}"
        else:
            req = req+f"{ip}:{port}"
        res = requests.post(req, json=pod)
        if res.status_code == 200:
            cmd = f" kubectl delete pod {podname} "
            subprocess.check_output(cmd, shell=True,universal_newlines=True)
            if podname not in foreignResources:
                abroadResources[podname]={"type":"pod","host":destination.split(":")[0], "API-port":destination.split(":")[1],"missed-heartbeat":0}
            else:
                p = foreignResources[podname]
                _ip = p["home-host"]
                _port = p["home-API-port"]
                freq = requests.patch(f"http://{_ip}:{_port}/resources/{podname}",json={"name":podname,"host":destination.split(":")[0], "API-port":destination.split(":")[1]})
                if freq.status_code == 200:
                    foreignResources.pop(podname)
#Move a deployment to a suitable cluster, then delete the deployment from the current cluster
def move_deployment(deploymentname):
    print(f"PR: {round(time.time() * 1000)}")
    dep = resources[deploymentname]
    destination = ask_controller(dep)
    print(f"Replication: {round(time.time() * 1000)}")
    service = hasAService(dep)
    d_req = "http://"+destination+"/deployments?home_address="
    if deploymentname in foreignResources:
            d_req = d_req+f"{foreignResources[deploymentname]['home-host']}:{foreignResources[deploymentname]['home-API-port']}"
    else:
        d_req = d_req+f"{ip}:{port}"
    d_req = requests.post(d_req, json=dep)
    if service:
        cmd = f" kubectl get service {service} -o json "
        ser =json.loads(subprocess.check_output(cmd, shell=True,universal_newlines=True))
        if "annotations" in ser["metadata"]:
            del ser["metadata"]["annotations"]
        s_req = "http://"+destination+"/services?home_address="
        if service in foreignResources:
            s_req = s_req+f"{foreignResources[service]['home-host']}:{foreignResources[service]['home-API-port']}"
        else:
            s_req = s_req+f"{ip}:{port}"
        s_req = requests.post(s_req, json=ser)
        if s_req.status_code == 200:

            print(f"Rerouting: {round(time.time() * 1000)}")
            f=open(f"/etc/nginx/sites-enabled/{service}.conf","w+")
            
            f.write("upstream "+service+" { server "+destination.split(":")[0]+":"+str(ser["spec"]["ports"][0]["nodePort"])+"; } server { listen "+str(ser["spec"]["ports"][0]["nodePort"])+"; location / { proxy_pass http://"+service+ "; } }")
            f.close()
            

    

            
            abroadResources[service]={"type":"service","host":destination.split(":")[0], "API-port":destination.split(":")[1],"missed-heartbeat":0}
    if d_req.status_code == 200:
        abroadResources[deploymentname]={"type":"deployment","host":destination.split(":")[0], "API-port":destination.split(":")[1],"missed-heartbeat":0}
    print(f"Waiting: {round(time.time() * 1000)}")
    while True:
        tmp=requests.get(f"http://{destination}/deployments/{deploymentname}")
        depl = tmp.json()
        if "availableReplicas" in depl['status']:
            print("***** Deleting home services *****")
            break
    print(f"Cleanup: {round(time.time() * 1000)}")
    subprocess.check_output("nginx -s reload", shell=True,universal_newlines=False)
    cmd = f" kubectl delete deployment {deploymentname} "
    subprocess.check_output(cmd, shell=True,universal_newlines=True)

    cmd = f" kubectl delete service {service} "
    subprocess.check_output(cmd, shell=True,universal_newlines=True)

    

#Send cluster labels to the meta-orchestrator
def updateOrchestrator(name,ip,port,labels,position):
    cmd = "top -bn2 | grep '%Cpu' | tail -1 | grep -P '(....|...) id,'|awk '{print 100-($8/4)}'"
    cpu =subprocess.check_output(cmd, shell=True,universal_newlines=False).decode("utf-8").replace("\n","").replace(",",".")
    cmd = "free | grep Mem | awk '{print $3/$2 * 100.0}'"
    mem =subprocess.check_output(cmd, shell=True,universal_newlines=False).decode("utf-8").replace("\n","").replace(",",".")
    channel.basic_publish(
        exchange='',
        routing_key='join_queue',
        body=json.dumps({"name":name,"ip":ip,"port":port,"labels":labels,"cpu":float(cpu),"mem":float(mem),"position":position}),
        properties=pika.BasicProperties(delivery_mode=pika.spec.PERSISTENT_DELIVERY_MODE))

#Thread in charge of performing periodical checks
def garbageCollector(abroadResources):
    count = 0
    while True:
        time.sleep(1)
        
        if abroadResources:
            print("AbroadResources: ")
            print(abroadResources)
        if foreignResources:
            print("ForeignResources: ")
            print(foreignResources)
        
        # -- Orchestrator update --
        if count == 5: #Every 10 sec send the labels to the meta-orchestrator
            count = 0
            updateOrchestrator(name,ip,port,getLabels(),position)
        # -- Check for pending pods and deployments --
        cmd = " kubectl get pods -o wide"
        tmp = subprocess.check_output(cmd, shell=True,universal_newlines=False).decode('utf-8').splitlines()
        
        for i,e in enumerate(tmp):
            if i != 0:
                status = " ".join(e.split()).split(" ")
                if status[2] == "Pending":
                    cmd = f" kubectl get pod {status[0]} -o json "
                    try:
                        pod = json.loads(subprocess.check_output(cmd, shell=True,universal_newlines=True))
                    except:
                        continue
                    creation = datetime.strptime(pod["metadata"]["creationTimestamp"],"%Y-%m-%dT%H:%M:%SZ")
                    now = datetime.now()
                    c_ts = datetime.timestamp(creation) + 3600 #somehow the time in creation timestamp is one hour behind
                    now_ts = datetime.timestamp(now)
                   
                
                    if  (now_ts-c_ts) > 10:
                        
                        dep = isADeploymentPod(pod["metadata"]["name"])
                        if dep != False:
                            move_deployment(dep)
                        else:
                            move_pod(pod["metadata"]["name"])
                        
                            
        # -- Check that pods and deployments in abroadResources are still alive in other clusters --
        ar = {}
        try:
            for e in abroadResources:
                if abroadResources[e]["host"] not in ar:
                    ar[abroadResources[e]["host"]] = []
                    res = requests.get("http://"+abroadResources[e]["host"]+":"+abroadResources[e]["API-port"]+"/pods",timeout=5)
                    if res.status_code == 200:
                        tmp = json.loads(res.content.decode('utf-8'))
                        for m in tmp["data"]:
                            if m.split(" ")[0] in abroadResources:
                                ar[abroadResources[e]["host"]].append(m.split(" ")[0])
                
                    res = requests.get("http://"+abroadResources[e]["host"]+":"+abroadResources[e]["API-port"]+"/deployments",timeout=5)
                    if res.status_code == 200:
                        tmp = json.loads(res.content.decode('utf-8'))
                        for m in tmp["data"]:
                            if m.split(" ")[0] in abroadResources:
                                ar[abroadResources[e]["host"]].append(m.split(" ")[0])

                    res = requests.get("http://"+abroadResources[e]["host"]+":"+abroadResources[e]["API-port"]+"/services",timeout=5)
                    if res.status_code == 200:
                        tmp = json.loads(res.content.decode('utf-8'))
                        for m in tmp["data"]:
                            if m.split(" ")[0] in abroadResources:
                                ar[abroadResources[e]["host"]].append(m.split(" ")[0])
            # -- Update the condition of the pods and deployments in aboradResources, delete the resource after missing 3 hb --       
            to_delete = []
            for pod in abroadResources:
                if pod in ar[abroadResources[pod]["host"]]:
                    abroadResources[pod]["missed-heartbeat"]=0
                if pod not in ar[abroadResources[pod]["host"]] and abroadResources[pod]["missed-heartbeat"] < 3:
                    abroadResources[pod]["missed-heartbeat"]+=1
                if pod not in ar[abroadResources[pod]["host"]] and abroadResources[pod]["missed-heartbeat"] >= 3:
                    to_delete.append(pod)
            for pod in to_delete:
                abroadResources.pop(pod)
                failsafe_redeploy(pod)
            count+=1
        except:
            print(f"Cluster {abroadResources[e]['host']} didn't reply, redeploying resource")
            dead_cluster = abroadResources[e]['host']
            to_delete = []
            print("REDEPLOYING RESOURCES FAILSAFE")
            print(f"Start Time: {round(time.time() * 1000)}")
            for e in abroadResources:
                if abroadResources[e]["host"] == dead_cluster:
                    failsafe_redeploy(e)
                    to_delete.append(e)
            print(f"End Time: {round(time.time() * 1000)}")
            for e in to_delete:
                del abroadResources[e]


def requiredHelper(expressions):
    composedExpr = []
    for e in expressions:
        for m in e["matchExpressions"]:
            operator = m["operator"]
            #Define specific operation per operator
            if operator == "In":
                for v in m["values"]:
                    composedExpr.append(f"{m['key']}={v}")

    if all(item in getLabels() for item in composedExpr):
        return True
    return False

def podHelper(pod):
    elegibleClusters = []
    deployable = True
    if "spec" in pod and "affinity" in pod["spec"] and "requiredDuringSchedulingIgnoredDuringExecution" in pod['spec']["affinity"]["nodeAffinity"]:
        expr =   pod['spec']["affinity"]["nodeAffinity"]["requiredDuringSchedulingIgnoredDuringExecution"]["nodeSelectorTerms"]
        deployable = requiredHelper(expr)
    return deployable
def deploymentHelper(depl):
    elegibleClusters = []
    deployable = True
    if   "affinity" in depl["spec"]["template"]["spec"] and "nodeAffinity" in depl["spec"]["template"]["spec"]["affinity"] and "requiredDuringSchedulingIgnoredDuringExecution" in depl["spec"]["template"]["spec"]["affinity"]["nodeAffinity"]:
        expr =   depl["spec"]["template"]["spec"]["affinity"]["nodeAffinity"]["requiredDuringSchedulingIgnoredDuringExecution"]["nodeSelectorTerms"]
        deployable = requiredHelper(expr)
    return deployable

def changeTag(depname,tag):
    try:
        cmd = f" kubectl get deployment {depname} -o json"
        depl = json.loads(subprocess.check_output(cmd, shell=True,universal_newlines=False))
    except:
        print("Deployment not present, can't change tag")
        return
    image = depl["spec"]["template"]["spec"]["containers"][0]["image"]
    print(image)
    new_image = f"{image.split(':')[0]}:{tag}"
    depl["spec"]["template"]["spec"]["containers"][0]["image"] = new_image
    if "annotations" in depl["metadata"]:
        del depl["metadata"]["annotations"]
    cmd =  f"echo '{json.dumps(depl)}' | kubectl apply -f - "
    
    subprocess.check_output(cmd, shell=True,universal_newlines=True)
    print("Changed happened without problems")

def failsafe_redeploy(resource):
    if resource in resources:
        if resources[resource]["kind"] == "Deployment":
            cmd = f"echo '{json.dumps(resources[resource])}' | kubectl apply -f - "
            result = subprocess.check_output(cmd, shell=True,universal_newlines=True)
        if resources[resource]["kind"] == "Service":
            f=open(f"/etc/nginx/sites-enabled/classifier-service.conf","w+")
            f.write("upstream classifier-service { server 192.168.49.2:"+str(resources[resource]["spec"]["ports"][0]["nodePort"])+"; } server { listen "+str(resources[resource]["spec"]["ports"][0]["nodePort"])+"; location / { proxy_pass http://"+resource +"; } }")
            f.close()
            cmd = f"echo '{json.dumps(resources[resource])}' | kubectl apply -f - "
            result = subprocess.check_output(cmd, shell=True,universal_newlines=True)
            subprocess.check_output("nginx -s reload", shell=True,universal_newlines=False)

            


def redeploy_classifier():
    if ("classifier" in resources and "classifier-service" in resources) and ("classifier" in abroadResources and "classifier-service" in abroadResources):
        print(" ***** Redeploying classifier *****")
        cmd = f"echo '{json.dumps(resources['classifier'])}' | kubectl apply -f - "
        result = subprocess.check_output(cmd, shell=True,universal_newlines=True)

        print(" ***** Redeploying service *****")
        ser = resources['classifier-service']
        cmd = f"echo '{json.dumps(ser)}' | kubectl apply -f - "
        result = subprocess.check_output(cmd, shell=True,universal_newlines=True)

        f=open(f"/etc/nginx/sites-enabled/classifier-service.conf","w+")
        f.write("upstream classifier-service { server 192.168.49.2:"+str(ser["spec"]["ports"][0]["nodePort"])+"; } server { listen "+str(ser["spec"]["ports"][0]["nodePort"])+"; location / { proxy_pass http://classifier-service; } }")
        f.close()
        while True:
            cmd = f" kubectl get deployment classifier -o json"
            depl = json.loads(subprocess.check_output(cmd, shell=True,universal_newlines=False))
            if "availableReplicas" in depl["status"]:
                print(" ***** Pods now available *****")
                break
            time.sleep(1)
        print(" ***** Reloading nginx configuration *****")
        subprocess.check_output("nginx -s reload", shell=True,universal_newlines=False)
        print(" ***** Deleting classifier abroad ***** ")
        try:
            requests.delete("http://"+abroadResources["classifier"]["host"]+":"+abroadResources["classifier"]["API-port"]+"/deployments/classifier")
            requests.delete("http://"+abroadResources["classifier-service"]["host"]+":"+abroadResources["classifier-service"]["API-port"]+"/services/classifier-service")

            abroadResources.pop("classifier")
            abroadResources.pop("classifier-service")
        except:
            abroadResources.pop("classifier")
            abroadResources.pop("classifier-service")
            print("Can't reach abroad cluster")
    

def bandwidth_checker():
    global connection_ema,alpha, connection_status
    connection_status = 0
    mid_margin = 460800
    good_margin = 943718
    while True:
        time.sleep(1)
        try:
            f = open("bandwidth.log","r")
            res = f.read()
            bw = int(json.loads(res)["bw"])
        except:
            print("couldn't read json")

        if connection_ema==0:
            connection_ema = bw
            if bw >= 1048576:
                changeTag("streaming","good")
                connection_status=2
            if bw >= 512000 and bw < 1048576:
                changeTag("streaming","mid")
                connection_status=1
            if bw < 512000:
                changeTag("streaming","bad")
                connection_status=0
        else:
            connection_ema = alpha * bw + (1 - alpha) * connection_ema
        
        if connection_ema < 512000 and connection_status != 0:
            if (connection_status == 1 and connection_ema < mid_margin):
                try:
                    connection_status = 0
                    changeTag("streaming","bad")
                    print("Connection status changed to: bad")
                except:
                    print("No streaming")

        if connection_ema >= 512000 and connection_ema < 1048576 and connection_status != 1:
            if (connection_status == 2 and connection_ema < good_margin) or connection_status == 0:
                    if connection_status == 2:
                            redeploy_classifier()
                    changeTag("streaming","mid")
                    print("Connection status changed to: mid")
                    connection_status = 1
        if connection_ema >= 1048576 and connection_status != 2:
            if connection_status == 1:
                connection_status = 2
                changeTag("streaming","good")
                print(f"Start Time: {round(time.time() * 1000)}")
                move_deployment("classifier")
                print(f"End Time: {round(time.time() * 1000)}")
                print("Connection status changed to: good")


    


channel.basic_consume(
            queue=callback_queue,
            on_message_callback=rpcResponse,
            auto_ack=True)  
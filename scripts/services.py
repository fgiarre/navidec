import subprocess
import json
import pika
import time
import requests
import os
from scripts.config import *

def services_get(request):
    cmd = " kubectl get services -o wide"
    tmp = subprocess.check_output(cmd, shell=True,universal_newlines=False).decode('utf-8').splitlines()
    result = {"data":[]}
    for e in tmp:
        result["data"].append(" ".join(e.split()))
    
    if request.args.get("abroad") == "True":

        ar_set = set()
        for e in abroadResources:
            if abroadResources[e]["host"] not in ar_set:
                ar_set.add(abroadResources[e]["host"])
                res = requests.get("http://"+abroadResources[e]["host"]+":"+abroadResources[e]["API-port"]+"/services")
                if res.status_code == 200:
                    tmp = json.loads(res.content.decode('utf-8'))
                    for m in tmp["data"]:
                        if m.split(" ")[0] in abroadResources:
                            result["data"].append(m)
            
    return f"{json.dumps(result)}",200

def services_add(request):
    result=""
    try:
        content = json.loads(json.dumps(request.data.decode('utf-8')))
        cmd = f"echo '{content}' | kubectl apply -f - "
        result = subprocess.check_output(cmd, shell=True,universal_newlines=True)

    except:
        content = request.data.decode('utf-8')
        cmd = f" kubectl create -f {content} "
        result = subprocess.check_output(cmd, shell=True,universal_newlines=True)

    service = result.split(" ")[0].split("/")[1]
    cmd = f" kubectl get service {service} -o json"
    ser = json.loads(subprocess.check_output(cmd, shell=True,universal_newlines=True))
    if "annotations" in ser["metadata"]:
        print("Last config present")
        del ser["metadata"]["annotations"]
    resources[service]=ser

    f=open(f"/etc/nginx/sites-enabled/{service}.conf","w+")
    f.write("upstream "+service+" { server 192.168.49.2:"+str(ser["spec"]["ports"][0]["nodePort"])+"; } server { listen "+str(ser["spec"]["ports"][0]["nodePort"])+"; location / { proxy_pass http://"+service+ "; } }")
    f.close()
    subprocess.check_output("nginx -s reload", shell=True,universal_newlines=False)

    if "home_address" in request.args:
        home = request.args.get("home_address")
        ip = home.split(":")[0]
        port = home.split(":")[1]
        name = result.split("/")[1].split(" ")[0]
        foreignResources[name] = {"type":"service","home-host":ip,"home-API-port":port}

    return result,200

def service_delete(name):
    if name not in abroadResources:
        cmd = f" kubectl delete service {name} "
        result = subprocess.check_output(cmd, shell=True,universal_newlines=True)
        os.remove(f"/etc/nginx/sites-enabled/{name}.conf")
       

    else:
        res = requests.delete("http://"+abroadResources[name]["host"]+":"+abroadResources[name]["API-port"]+"/services/"+name)
        result = res.content.decode('utf-8')
    if name in foreignResources:
        foreignResources.pop(name)
    return f"{result}",200
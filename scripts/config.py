import pika
import uuid
import os

name = os.getenv('CLUSTERNAME')
ip = os.getenv('API_IP')
port="5000"
position = os.getenv('POSITION')

print(f"APIs for cluster {name} listening on {ip}:5000. Cluster positioned on the {position}")


connection_status=0 #start considering bad 
connection_ema = 0 #array for the exponential moving average
alpha = 0.05 #decaying factor for the ema [0,1[, high alpha -> care less about the past ; low alpha -> care more about the past


abroadResources={} #keeps track of resources deployed abroad
foreignResources={} #keeps track of resources deployed for other clusters
availableLabels={} #keeps track of labels of the different nodes
resources={} #keeps track of the resources used for deploying a certain resource


rpc_result = ""
corr_id = str(uuid.uuid4())

print(f"Connecting on broker at {os.getenv('BROKER_IP')}")
credentials = pika.PlainCredentials('navidec', 'navidec')
connection = pika.BlockingConnection(pika.ConnectionParameters(host=os.getenv("BROKER_IP"),credentials=credentials))
channel = connection.channel()
channel.queue_declare(queue='join_queue')
result = channel.queue_declare(queue='', exclusive=True)
callback_queue = result.method.queue


import subprocess
import json
import pika
import time
import requests
from scripts.config import *
from scripts.utils import *

#Get all deployments - Adding the "abroad" argument will also retrieve deployments registered in abroadResources
def deployments_get(request):
    cmd = " kubectl get deployments -o wide"
    tmp = subprocess.check_output(cmd, shell=True,universal_newlines=False).decode('utf-8').splitlines()
    result = {"data":[]}
    for e in tmp:
        result["data"].append(" ".join(e.split()))
    
    if request.args.get("abroad") == "True":
        ar_set = set()
        for e in abroadResources:
            if abroadResources[e]["host"] not in ar_set:
                ar_set.add(abroadResources[e]["host"])
                res = requests.get("http://"+abroadResources[e]["host"]+":"+abroadResources[e]["API-port"]+"/deployments")
                if res.status_code == 200:
                    tmp = json.loads(res.content.decode('utf-8'))
                    for m in tmp["data"]:
                        if m.split(" ")[0] in abroadResources:
                            result["data"].append(m)
            
    return f"{json.dumps(result)}",200

def deployment_get(name):
    cmd = f"kubectl get deployment {name} -o json"
    return json.loads(subprocess.check_output(cmd, shell=True,universal_newlines=True))

#Add a new deployment by file, json or internet resource
def deployment_add(request):
    global foreignResources,resources
    result = ""
    try:
        content = json.loads(json.dumps(request.data.decode('utf-8')))
        cmd = f"echo '{content}' | kubectl apply -f - "
        result = subprocess.check_output(cmd, shell=True,universal_newlines=True)
        
    except:
        content = request.data.decode('utf-8')
        cmd = f" kubectl create -f {content} "
        result = subprocess.check_output(cmd, shell=True,universal_newlines=True)
        
    deployment_name = result.split(" ")[0].split("/")[1]
    cmd = f" kubectl get deployment {deployment_name} -o json"
    depl = json.loads(subprocess.check_output(cmd, shell=True,universal_newlines=False))
    if "annotations" in depl["metadata"]:
        print("Last config present")
        del depl["metadata"]["annotations"]
    resources[deployment_name] = depl
    if "home_address" in request.args:
        home = request.args.get("home_address")
        ip = home.split(":")[0]
        port = home.split(":")[1]
        name = result.split("/")[1].split(" ")[0]
        foreignResources[name] = {"type":"deployment","home-host":ip,"home-API-port":port}
    
    if not deploymentHelper(depl):
        move_deployment(deployment_name)


    return result,200

#Delete a deployment by name
def deployment_delete(name):
    if name not in abroadResources:
        cmd = f" kubectl delete deployment {name} "
        result = subprocess.check_output(cmd, shell=True,universal_newlines=True)
    else:
        res = requests.delete("http://"+abroadResources[name]["host"]+":"+abroadResources[name]["API-port"]+"/deployments/"+name)
        result = res.content.decode('utf-8')
    if name in foreignResources:
        foreignResources.pop(name)
    return f"{result}",200

def deployment_move_affinity(name,request):
    content = json.loads(request.data.decode('utf-8'))
    if name in abroadResources:
        result = requests.post("http://"+abroadResources[name]["host"]+":"+abroadResources[name]["API-port"]+"/deployments/"+name+"/move_affinity",json=content)
    else:
        expressions = content['matchExpressions']
        
        cmd = f" kubectl get deployment {name} -o json"
        deployment = json.loads(subprocess.check_output(cmd, shell=True,universal_newlines=True))
        dep = deployment["spec"]["template"]
        if 'nodeName' in dep['spec']:
            dep['spec'].pop('nodeName')
        
        if 'affinity' in dep['spec']:
            dep['spec'].pop('affinity')
        affinity = {"nodeAffinity":{}}
        if 'type' in content and content['type']=="required":
            affinity["nodeAffinity"] = {"requiredDuringSchedulingIgnoredDuringExecution":{"nodeSelectorTerms":[{"matchExpressions":expressions}]}}
        if 'type' in content and content['type']=="preferred":
            affinity["nodeAffinity"] = {"preferredDuringSchedulingIgnoredDuringExecution":[{"weight":1,"preference":{"matchExpressions":expressions}}]}
        dep['spec']['affinity'] = affinity
        cmd = f" kubectl delete deployment {name}"
        subprocess.check_output(cmd, shell=True,universal_newlines=True)
        cmd =  f"echo '{json.dumps(deployment)}' | kubectl apply -f - "
        result = subprocess.check_output(cmd, shell=True,universal_newlines=True)
        
    return f"{result}",200


